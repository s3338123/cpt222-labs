package com.herron;

import com.herron.lab1.Flyable;
import com.herron.lab1.bird.Eagle;
import com.herron.lab1.bird.Parrot;
import com.herron.lab1.hero.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Luke Herron
 */
public class Main {

    private Main() { }

    public static void main(String[] args) {
        List<SuperHero> superHeroes = new ArrayList<>(Arrays.asList(
                new Aquaman(), new Batman(), new Birdman(), new Superman(), new WonderWoman()));

        List<Flyable> flyables = new ArrayList<>(Arrays.asList(
                new Eagle(), new Parrot(), new Superman(), new Birdman()));

        superHeroes.forEach(SuperHero::saveTheWorld);
        flyables.forEach(Flyable::fly);
    }
}
