package com.herron.lab1.bird;

import com.herron.lab1.Flyable;

/**
 * @author Luke Herron
 */
public class Eagle extends AbstractBird implements Flyable {

    private static final String NAME = "Eagle";

    public Eagle() {
        super(NAME);
    }

    @Override
    public void fly() {
        System.out.println("The " + name + " is soaring");
    }
}
