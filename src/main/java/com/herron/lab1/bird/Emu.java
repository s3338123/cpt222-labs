package com.herron.lab1.bird;

/**
 * @author Luke Herron
 */
public class Emu extends AbstractBird {

    private static final String NAME = "Emu";

    public Emu() {
        super(NAME);
    }
}
