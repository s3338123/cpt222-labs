package com.herron.lab1.bird;

/**
 * @author Luke Herron
 */
public class Penguin extends AbstractBird {

    private static final String NAME = "Penguin";

    public Penguin() {
        super(NAME);
    }
}
