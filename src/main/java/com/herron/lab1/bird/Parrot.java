package com.herron.lab1.bird;

import com.herron.lab1.Flyable;

/**
 * @author Luke Herron
 */
public class Parrot extends AbstractBird implements Flyable {

    private static final String NAME = "Parrot";

    public Parrot() {
        super(NAME);
    }

    @Override
    public void fly() {
        System.out.println("The " + name + " is flying in all its glorified colour");
    }
}
