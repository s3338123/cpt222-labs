package com.herron.lab1.bird;

/**
 * @author Luke Herron
 */
public abstract class AbstractBird {

    protected String name;

    public AbstractBird(String name) {
        this.name = name;
    }
}
