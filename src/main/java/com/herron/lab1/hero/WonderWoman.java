package com.herron.lab1.hero;

/**
 * @author Luke Herron
 */
public class WonderWoman extends AbstractSuperHero {

    private static final String NAME = "WonderWoman";

    public WonderWoman() {
        super(NAME);
    }
}
