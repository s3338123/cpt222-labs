package com.herron.lab1.hero;

/**
 * @author Luke Herron
 */
@FunctionalInterface
public interface SuperHero {
    
    void saveTheWorld();
}
