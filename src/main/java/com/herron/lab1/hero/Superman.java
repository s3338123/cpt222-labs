package com.herron.lab1.hero;

import com.herron.lab1.Flyable;

/**
 * @author Luke Herron
 */
public class Superman extends AbstractSuperHero implements Flyable {

    private static final String NAME = "Superman";

    public Superman() {
        super(NAME);
    }

    @Override
    public void fly() {
        System.out.println(name + " is flying to the fortress of solitude");
    }
}
