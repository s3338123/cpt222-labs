package com.herron.lab1.hero;

import com.herron.lab1.Flyable;

/**
 * @author Luke Herron
 */
public class Birdman extends AbstractSuperHero implements Flyable {

    private static final String NAME = "Birdman";

    public Birdman() {
        super(NAME);
    }

    @Override
    public void fly() {
        System.out.println(name + " is flying close to the sun");
    }
}
