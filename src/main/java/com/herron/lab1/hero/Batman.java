package com.herron.lab1.hero;

/**
 * @author Luke Herron
 */
public class Batman extends AbstractSuperHero {

    private static final String NAME = "Batman";

    public Batman() {
        super(NAME);
    }

    @Override
    public void saveTheWorld() {
        System.out.println(name + " saves the world again! Gotham city celebrates!");
    }
}
