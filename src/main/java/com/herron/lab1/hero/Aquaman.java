package com.herron.lab1.hero;

/**
 * @author Luke Herron
 */
public class Aquaman extends AbstractSuperHero {

    private static final String NAME = "Aquaman";

    public Aquaman() {
        super(NAME);
    }

    @Override
    public void saveTheWorld() {
        System.out.println(name + " saves the world again! Atlantis rejoices!");
    }
}
