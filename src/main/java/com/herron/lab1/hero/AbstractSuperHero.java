package com.herron.lab1.hero;

/**
 * @author Luke Herron
 */
public abstract class AbstractSuperHero implements SuperHero {

    protected String name;

    AbstractSuperHero(String name) {
        this.name = name;
    }

    public void saveTheWorld() {
        System.out.println(name + " saves the world again!");
    }
}
