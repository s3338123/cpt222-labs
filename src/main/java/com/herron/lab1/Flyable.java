package com.herron.lab1;

/**
 * @author Luke Herron
 */
@FunctionalInterface
public interface Flyable {

    void fly();
}
