package com.herron.lab1.bird;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Luke Herron
 */
public class EagleTest {

    private PrintStream oldStdOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setupStreams() {
        oldStdOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void shouldReturnEagleName() {
        Eagle eagle = new Eagle();

        Assert.assertEquals("Eagle", eagle.name);
    }

    @Test
    public void flyMethodShouldReturnEagleName() {
        Eagle eagle = new Eagle();
        eagle.fly();

        Assert.assertEquals("The Eagle is soaring", outContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(oldStdOut);
    }
}
