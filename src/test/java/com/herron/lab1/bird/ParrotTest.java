package com.herron.lab1.bird;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Luke Herron
 */
public class ParrotTest {

    private PrintStream oldStdOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setupStreams() {
        oldStdOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void shouldReturnParrotName() {
        Parrot parrot = new Parrot();

        Assert.assertEquals("Parrot", parrot.name);
    }

    @Test
    public void flyMethodShouldReturnParrotName() {
        Parrot parrot = new Parrot();
        parrot.fly();

        Assert.assertEquals("The Parrot is flying in all its glorified colour", outContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(oldStdOut);
    }
}
