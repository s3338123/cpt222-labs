package com.herron.lab1.bird;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luke Herron
 */
public class EmuTest {

    @Test
    public void shouldReturnEmuName() {
        Emu emu = new Emu();

        Assert.assertEquals("Emu", emu.name);
    }
}
