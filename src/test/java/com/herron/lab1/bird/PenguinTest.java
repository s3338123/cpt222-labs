package com.herron.lab1.bird;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luke Herron
 */
public class PenguinTest {

    @Test
    public void shouldReturnPenguinTest() {
        Penguin penguin = new Penguin();

        Assert.assertEquals("Penguin", penguin.name);
    }
}
