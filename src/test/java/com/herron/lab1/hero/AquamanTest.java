package com.herron.lab1.hero;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Luke Herron
 */
public class AquamanTest {

    private final Aquaman aquaman = new Aquaman();
    private PrintStream oldStdOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setupStreams() {
        oldStdOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void aquamanShouldSaveTheWorldWhileAtlantisRejoices() {
        aquaman.saveTheWorld();

        Assert.assertEquals("Aquaman saves the world again! Atlantis rejoices!", outContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(oldStdOut);
    }
}
