package com.herron.lab1.hero;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Luke Herron
 */
public class SupermanTest {

    private final Superman superman = new Superman();
    private PrintStream oldStdOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setupStreams() {
        oldStdOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void supermanShouldSaveTheWorld() {
        superman.saveTheWorld();

        Assert.assertEquals("Superman saves the world again!", outContent.toString().trim());
    }

    @Test
    public void supermanShouldFlyToFortress() {
        superman.fly();

        Assert.assertEquals("Superman is flying to the fortress of solitude", outContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(oldStdOut);
    }
}
