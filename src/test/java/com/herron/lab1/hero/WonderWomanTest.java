package com.herron.lab1.hero;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Luke Herron
 */
public class WonderWomanTest {

    private final WonderWoman wonderWoman = new WonderWoman();
    private PrintStream oldStdOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setupStreams() {
        oldStdOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void wonderWomanShouldSaveTheWorld() {
        wonderWoman.saveTheWorld();

        Assert.assertEquals("WonderWoman saves the world again!", outContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(oldStdOut);
    }
}
