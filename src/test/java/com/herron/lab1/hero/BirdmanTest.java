package com.herron.lab1.hero;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Luke Herron
 */
public class BirdmanTest {

    private final Birdman birdman = new Birdman();
    private PrintStream oldStdOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setupStreams() {
        oldStdOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void birdmanShouldSaveTheWorld() {
        birdman.saveTheWorld();

        Assert.assertEquals("Birdman saves the world again!", outContent.toString().trim());
    }

    @Test
    public void birdmanShouldFlyCloseToTheSun() {
        birdman.fly();

        Assert.assertEquals("Birdman is flying close to the sun", outContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(oldStdOut);
    }
}
